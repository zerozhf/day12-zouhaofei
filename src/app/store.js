import { configureStore } from '@reduxjs/toolkit'
import toDoListSlice from '../component/toDoListSlice'

export default configureStore({
  reducer: {toDo: toDoListSlice},
})