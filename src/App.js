
import ToDoList from './component/ToDoList';
import AddToDo from './component/AddToDo';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>ToDo List</h1>
      <ToDoList />
      <AddToDo  />
    </div>
  );
}

export default App;
