import ToDo from "./ToDo";
import { useSelector } from "react-redux"
const ShowList = () =>{
    const toDoList = useSelector(state => state.toDo.toDoList)
    return(
        <div>
            <ul>
             {toDoList.map((toDoItem) => (<ToDo key={toDoItem.id} toDoItem = {toDoItem} />))}
           </ul>
        </div>
    )
}

export default ShowList;