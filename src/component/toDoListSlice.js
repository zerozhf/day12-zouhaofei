import { createSlice } from '@reduxjs/toolkit'
import { nanoid } from 'nanoid';

const toDoListSlice = createSlice({
    name: 'todo',
    initialState: {
        toDoList: []
    },
    reducers: {
        addToDo: (state, action) => {
            const toDoItem = {
                text: action.payload,
                id: nanoid(),
                done: true
            };
            state.toDoList  = [...state.toDoList,toDoItem];
        },
        deleteToDo: (state, action) => {
            const id = action.payload;
            state.toDoList = state.toDoList.filter((todo) => {
                return !id.includes(todo.id)
            });
        },

        switchToDoDone: (state, action) => {
            const id = action.payload;
            state.toDoList = state.toDoList.map((todo) => {
                if(todo.id === id){
                    todo.done = !todo.done
                }
                return todo;
            });
        },
    }
})

export const { addToDo,deleteToDo,switchToDoDone } = toDoListSlice.actions

export default toDoListSlice.reducer