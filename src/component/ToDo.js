import { deleteToDo,switchToDoDone } from "./toDoListSlice";
import { useDispatch} from "react-redux"
import "../button.css";
const ToDo = (props) => {
    const {toDoItem} = props;
    const dispatch = useDispatch();
    const deleteTodoTtem = () =>{
        dispatch(deleteToDo(toDoItem.id));
    }
    const handleSwitchDoneState = () =>{
        dispatch(switchToDoDone(toDoItem.id));
    }
    const img = require("../R-C.jpg")
    return(
        <div >
            <span style={{textDecoration: toDoItem.done ? "none" : "line-through"}} onClick={handleSwitchDoneState} id="button">{toDoItem.text}</span>
            <span  onClick={deleteTodoTtem}  value="delete" > <img src={img} alt=" logo" id="image"></img></span>
        </div>

    );
}

export default ToDo;