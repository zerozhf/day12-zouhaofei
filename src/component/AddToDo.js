import { useState } from 'react';
import {useDispatch} from "react-redux"
import {addToDo} from "./toDoListSlice"
const AddToDo = () => {
    const [inputData, setInputData] = useState('');
    const dispatch = useDispatch();
    const handleInputChange = (event) => {
        setInputData(event.target.value);
    };

    const handleSubmit = () => {
        dispatch(addToDo(inputData));
        setInputData('');
    };
    return (
        <div>
            <input
                type="text"
                value={inputData}
                onChange={handleInputChange}
            />
            <button onClick={handleSubmit}>提交</button>
        </div>
    )

}

export default AddToDo;