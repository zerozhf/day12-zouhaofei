# Daily Report (2023/07/25)
# O
### code review
#### In today's Code Review session, I learned a new knowledge point. When traversing a list, I need a key. Previously, I used to directly use index as the key, but when performing delete operations, there were errors, so I needed to use a unique ID as the key.
### redux
#### Redux: I spent the whole day using Redux to refactor yesterday's exercises and assignments. During the refactoring process, I realized the advantages of Redux. Redux can make React's data transmission no longer limited to one-way, reducing many tedious parameter transfer operations, making the code more concise and easy to read.
# R
meaningful
# I
* The most meaningful activity is Redux.
* I am not very familiar with the grammar of Redux and often consult materials during practice.
* I am currently limited to using Redux and am not familiar with the underlying principles of some
# D
Using Redux for data transfer.